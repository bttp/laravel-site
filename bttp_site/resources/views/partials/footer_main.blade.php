<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-lg-3">
                <div class="widget">
                    <h4>Get in touch with us</h4>
                    <strong>
                        <address class="titlebox">
                            Bet The Player Inc<br>
                            BTTP suite room V124, DB 91<br>
                            Someplace 71745 Earth </address>
                        <p class="titlebox">
                            <i class="icon-phone"></i> (123) 456-7890 - (123) 555-7891 <br>
                            <i class="icon-envelope-alt"></i> email@domainname.com
                        </p></strong>
                </div>
            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="widget">
                    <h4>Information</h4>
                    <strong>
                        <address class="titlebox">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</address>
                    </strong>
                </div>

            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="widget">
                    <h4>Pages</h4>
                    <ul class="link-list">
                        {{--*/ $cont = 0 /*--}}
                        @foreach($contents as $content)
                            @if($content->type==2)
                                @if($cont<4)
                                    <li><a href="{{route('page', ['id'=>$content->id,'page'=>str_slug($content->title,'-')])}}">{{$content->title}}</a></li>    
                                @endif
                                {{--*/ $cont++ /*--}}
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-sm-3 col-lg-3">
                <div class="widget">
                    <h4>Newsletter</h4>
                    <p>Fill your email and sign up for monthly newsletter to keep updated</p>
                    <div class="form-group multiple-form-group input-group">
                        <input type="email" name="email" class="form-control">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-theme btn-add">Subscribe</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="copyright">
                        <p>
							<span>� Bet The Player 2016 All right reserved. | <a href="#">Cronapi's</a> by Cronapi's Team
						</span></p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <ul class="social-network">
                        <li><a href="#" data-placement="top" title="" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" data-placement="top" title="" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" data-placement="top" title="" data-original-title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#" data-placement="top" title="" data-original-title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#" data-placement="top" title="" data-original-title="Google plus"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<a href="#" class="scrollup" style="display: block;"><i class="fa fa-angle-up active"></i></a>