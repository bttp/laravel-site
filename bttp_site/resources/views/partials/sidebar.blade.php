<div class="header-rightbar datagrid">
    <h3 class="title-section-rb">RESULTADOS<span> EN VIVO</span></h3>
    <h3 class="title-table-rb">BEISBOL</h3>
    <table>
        <tbody>
        <tr>
            <td>Red Sox VS Blue Jays</td>
            <td class="result">7-3</td>
            <td class="live">En Vivo</td>
        </tr>
        <tr>
            <td>Red Sox VS Blue Jays</td>
            <td class="retult"></td>
            <td class="time">7:00pm</td>
        </tr>
        <tr>
            <td>Red Sox VS Blue Jays</td>
            <td class="result">7-3</td>
            <td class="end">Finalizado</td>
        </tr>
        <tr>
            <td>Red Sox VS Blue Jays</td>
            <td class="result">7-3</td>
            <td class="end">Finalizado</td>
        </tr>
        <tr>
            <td>Red Sox VS Blue Jays</td>
            <td class="result">7-3</td>
            <td class="live">En Vivo</td>
        </tr>
        </tbody>
    </table>
    <h3 class="title-table-rb">FÚTBOL</h3>
    <table>
        <tbody>
        <tr>
            <td>R. Madrid VS Barcelona</td>
            <td class="result">1-2</td>
            <td class="live">En Vivo</td>
        </tr>
        <tr>
            <td>R. Madrid VS Barcelona</td>
            <td class="result">1-2</td>
            <td class="live">En Vivo</td>
        </tr>
        <tr>
            <td>R. Madrid VS Barcelona</td>
            <td class="result"></td>
            <td class="time">7:00pm</td>
        </tr>
        <tr>
            <td>R. Madrid VS Barcelona</td>
            <td class="result">1-2</td>
            <td class="live">En Vivo</td>
        </tr>
        <tr>
            <td>R. Madrid VS Barcelona</td>
            <td class="result">1-2</td>
            <td class="end">Finalizado</td>
        </tr>
        </tbody>
    </table>

    <h3 class="footer-section-rb"></h3>
    <br>
    <h3 class="title-section-rb">ÚLTIMAS NOTICIAS</h3>
    <table>
        <tbody>
        {{--*/ $cont = 0 /*--}}
        @foreach($contents as $content)
            @if($content->type==1)
                @if($cont<3)
                <tr>
                    <td><a href="{{route('page', ['id'=>$content->id,'page'=>str_slug($content->title,'-')])}}"><img src="{{URL::to('/img/upload/content/Content-'.$content->id.'.jpg')}}"/></a></td>
                    <td class="description" ><a href="{{route('page', ['id'=>$content->id,'page'=>str_slug($content->title,'-')])}}">{{$content->description}}</a></td>
                </tr>
                @endif
                {{--*/ $cont++ /*--}}
            @endif
        @endforeach

        </tbody>
    </table>
</div>