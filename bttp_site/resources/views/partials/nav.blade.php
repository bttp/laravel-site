<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#menu1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Beat The Player</a>
        </div>
        @if(Auth::check())
            <div class="collapse navbar-collapse" id="menu1">
                <ul class="nav navbar-nav">
                    @if(Auth::user()->role_id==1)
                                <li><a href="{{route('admin_categories')}}">Categorías</a></li>
                                <li><a href="{{route('admin_tags')}}">Etiquetas</a></li>
                                <li><a href="{{route('admin_sliders')}}">Slider</a></li>
                                <li><a href="{{route('admin_events')}}">Eventos</a></li>
                                <li><a href="{{route('admin_contents')}}">Contenidos</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{route('admin_users')}}">Usuarios</a></li>
                        </li>
                    @endif
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">{{Auth::user()->first_name}} {{Auth::user()->last_name}} <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{route('logout')}}"><i class="fa fa-sign-out" aria-hidden="true"></i>
                                    Salir</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        @endif
    </div>
</nav>