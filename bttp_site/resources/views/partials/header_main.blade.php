<header>
    <div class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header" >
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src='{{URL::to('/img/logo.jpg')}}' alt=""  ></a>
            </div>
            <br><br><br>
            <div class="navbar-collapse collapse ">
                <ul class="nav navbar-nav">
                    <li class="{{$class}}"><a href="{{route('home')}}">Home</a></li>
                    @foreach($contents as $content)
                        {{$class=''}}
                        @if($content->type==0)

                        <li class="@if($page==str_slug($content->title,'-')) active @endif"><a href="{{route('page', ['id'=>$content->id,'page'=>str_slug($content->title,'-')])}}">{{$content->title}}</a></li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</header>
<!-- end header -->