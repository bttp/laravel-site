<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Laravel</title>
 
	{!! Html::style('assets/css/bootstrap.min.css') !!}
	{!! Html::style('plugins/flexslider/flexslider.css') !!}
	{!! Html::style('assets/css/cubeportfolio.min.css') !!}
	{!! Html::style('assets/css/red.css') !!}
	{!! Html::style('assets/css/style.css') !!}

 
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
 
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
@include('partials.header_main')
 
	@yield('content')
@include('partials.footer_main')
	<!-- Scripts -->
	{!! Html::script('assets/js/jquery.min.js') !!}
	{!! Html::script('assets/js/modernizr.custom.js') !!}
	{!! Html::script('assets/js/jquery.easing.1.3.js') !!}

	{!! Html::script('assets/js/bootstrap.min.js') !!}
	{!! Html::script('plugins/flexslider/jquery.flexslider-min.js') !!}
	{!! Html::script('plugins/flexslider/flexslider.config.js') !!}
	{!! Html::script('assets/js/jquery.appear.js') !!}
	{!! Html::script('assets/js/stellar.js') !!}
	{!! Html::script('assets/js/classie.js') !!}
	{!! Html::script('assets/js/jquery.cubeportfolio.min.js') !!}
	{!! Html::script('assets/js/animate.js') !!}
	{!! Html::script('assets/js/custom.js') !!}
@yield('script')
</body>
</html>