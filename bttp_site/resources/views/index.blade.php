@extends('templates.main')

@section('content')
		<!-- start slider -->
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<!-- Slider -->
			<div id="main-slider" class="main-slider flexslider">
				<ul class="slides">
					@foreach($sliders as $slider)
						<li>
							<img src="img/upload/slider/{{'Slider-'.$slider->id.'.jpg'}}" alt="" />
							<div class="flex-caption">
								<h3>{{$slider->title}}</h3>
								<p>{{$slider->description}}</p>
								<a href="{{$slider->reference}}" class="btn btn-theme">{{$slider->title_reference}}</a>
							</div>
						</li>
					@endforeach
				</ul>
			</div>
			<!-- end slider -->
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-md-9" role="main">
			<div class="row"> <!--SECTION 1-->
				<h2 class="titlebox">DONDE PERDIENDO GANAS</h2>
				<div>
					<blockquote>
								<div><img style="float: right; width: 290px;height: 150px" src="img/content/img_1.jpg" />Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
							</blockquote>
				</div>
			</div>
			<div class="row"><!--SECTION 2-->
				<div class="col-lg-12">
					<div id="filters-container" class="cbp-l-filters-button">
						<h3 class="titlebox">EVENTOS DESTACADOS</h3>
						<div data-filter="*" class="cbp-filter-item-active cbp-filter-item">Todos<div class="cbp-filter-counter"></div></div>
						@foreach($categories as $category)
							<div data-filter=".{{strtolower($category->name)}}" class="cbp-filter-item">{{$category->description}}<div class="cbp-filter-counter"></div></div>
						@endforeach
					</div>


					<div id="grid-container" class="cbp-l-grid-projects">
						<ul>
							@foreach($events as $event)
								<li class="cbp-item {{strtolower($event->category->name)}}">
									<div class="cbp-caption" style="height: 110px">
										<div class="cbp-caption-defaultWrap">
											<img src="img/upload/event/Event-{{$event->id.'.jpg'}}"  />
										</div>
										<div class="cbp-caption-activeWrap">
											<div class="cbp-l-caption-alignCenter">
												<div class="cbp-l-caption-body">
													<a href="{{$event->reference}}" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="Chile campeón de la Copa">{{$event->title_reference}}</a>
												</div>
											</div>
										</div>
									</div>
									<div class="cbp-l-grid-projects-title titlebox">{{$event->title}}</div>
									<div class="cbp-l-grid-projects-desc" align="justify" ><p>{{$event->description}}</p></div>
								</li>
							@endforeach
						</ul>
					</div>

					<div class="cbp-l-loadMore-button">
						<a href="#" class="cbp-l-loadMore-button-link">Más Noticias</a>
					</div>

				</div>
			</div>
			<div class="row"><!--SECTION 3-->
				<div class="col-md-12" >
					<img src="img/content/img_7.jpg" style="max-width: 100%">
				</div>
			</div>
			<div class="row"><!--SECTION 4-->
				<div class="col-md-12" >
					<img src="img/content/img_8.jpg" style="max-width: 100%">
				</div>
			</div>

		</div>

		<div class="col-md-3" role="complementary" ><!--RIGHT COLUMN id="sidebar"-->
			@include('partials.sidebar')
		</div>
	</div>
</div>


@endsection