@extends('templates.main')
@section('script')
    <script>
        var html='{{$content->content}}';
        $(document).ready(function () {
            $urlimg=$('#myimg').data('file');
            $.ajax({
                url:$urlimg,
                type:'HEAD',
                error: function()
                {
                    $('#main').html('')

                },
                success: function()
                {

                }
            });

            myhtml=$('#main').html()
            $('#main').html(myhtml+$('#main').data('xhtml'));
            $('#main').attr('data-xhtml','');


        });

    </script>
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div id="main" data-xhtml='{{$content->content}}' class="@if($content->sidebar==1) col-md-9 @else col-md-12 @endif main-slider flexslider" role="main">
                <div class="slides ">
                    <img id="myimg" data-file="{{URL::to('/img/upload/content/Content-'.$content->id.'.jpg')}}" src="{{URL::to('/img/upload/content/Content-'.$content->id.'.jpg')}}" alt="" />
                </div>
                <h1 class="titlebox">{{$content->title}}</h1>
            </div>
            @if($content->sidebar==1)
                <div id="complement" style="margin-top: 4%;" class="col-md-3" role="complementary" >
                    @include("partials.sidebar")
                </div>
            @endif
        </div>
    </div>
@endsection
