@extends('templates.admin')
@section('script')
    <script>
        var urlUpdate = '{{route('update_event')}}';
        var token='{{csrf_token()}}';
        $(document).ready(function () {
        });
        $(document).on("click", "#registrar", function (e) {
            e.preventDefault();
            if (!confirm("¿Está seguro de registrar éste Evento?")){
                return;
            }
            var fileInput = $("#image")[0],
                    file = fileInput.files && fileInput.files[0];

            /*if( file ) {
             var img = new Image();

             img.src = window.URL.createObjectURL( file );

             img.onload = function() {
             var width = img.naturalWidth,
             height = img.naturalHeight;

             window.URL.revokeObjectURL( img.src );

             if( width == 400 && height == 300 ) {
             form.submit();
             }
             else {
             alert("La imagen debe poseer las siguientes dimensiones: ");
             return;
             }
             };
             }else{*/
            $("#register").submit();
            /*}*/
        });

        $(document).on("click", ".delete", function(e){
            if (!confirm('¿Está seguro de eliminar éste Evento?')){
                e.preventDefault();
                return;
            }
        });

        $(document).on("click", ".imagen", function(){
            img = $(this).data("img");
            $("#img").attr('src', img);
            $("#modal_image").modal();
        });
    </script>
    {!! Html::script('assets/js/adminEvent.js') !!}
@endsection
@section('content')
    @if(Session::has('mensaje'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('mensaje')}}
        </div>
    @endif
    <div class="panel panel-default">
        <div class="panel-heading">Administrar Eventos</div>
        <div class="panel-body">
            <form id="register" action="{{route('create_event')}}" method="post" enctype="multipart/form-data">
                <h3>Registro</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">Título</label>
                            <input type="text" name="title" id="title" class="form-control" placeholder="Título" value="{{old('title')}}">
                            @if ($errors->get('title'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('title') as $error)
                                        <strong>Error:</strong>{{$error}}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="description">Descripción</label>
                            <textarea name="description" id="description" class="form-control" rows="3">{{old('description')}}</textarea>
                            @if ($errors->get('description'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('description') as $error)
                                        <strong>Error:</strong>{{$error}}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title_reference">Título de Referencia</label>
                            <input type="text" name="title_reference" id="title_reference" class="form-control" placeholder="Título de referencia" value="{{old('title_reference')}}">
                            @if ($errors->get('title_reference'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('title_reference') as $error)
                                        <strong>Error:</strong>{{$error}}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="reference">Referencia</label>
                            <input type="text" name="reference" id="reference" class="form-control"
                                   placeholder="Referencia" value="{{old('reference')}}">
                            @if ($errors->get('reference'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('reference') as $error)
                                        <strong>Error:</strong>{{$error}}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="image">Imagen</label>
                            <input type="file" name="image" id="image" class="form-control" value="{{old('image')}}">
                            @if ($errors->get('image'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('image') as $error)
                                        <strong>Error:</strong>{{$error}}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="category_id">Categoría</label>
                            <select name="category_id" id="category_id" class="form-control">
                                <option value="">Seleccione...</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}" @if(old('category_id')==$category->id) selected="selected" @endif>{{$category->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->get('category_id'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('category_id') as $error)
                                        <strong>Error:</strong>{{$error}}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    <div class="col-md-3 col-md-offset-6">
                        <input type="submit" id="registrar" value="Registrar" class="btn btn-success">
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <table id="asignaturas" class="table table-responsive table-stripped table-hover" style="width: 100%;table-layout: fixed;">
                        <thead>
                        <tr>
                            <th style="width: 20px">#</th>
                            <th style="width: 10%;">Categoría</th>
                            <th>Título</th>
                            <th>Descripción</th>
                            <th>Título de Referencia</th>
                            <th>Referencia</th>
                            <th>Imagen</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($events as $event)
                            <tr>
                                <td >{{$event->id}}</td>
                                <td style="word-break: break-all;">{{$event->category->name}}</td>
                                <td style=";width: 10%;">{{$event->title}}</td>
                                <td style="width: 20%;">{{$event->description}}</td>
                                <td style="width: 10%;">{{$event->title_reference}}</td>
                                <td style="word-break: break-all;width: 20%;">{{$event->reference}}</td>
                                <td><a class="btn btn-primary imagen" data-img="{{route('event_image', ['filename' => 'Event-'.$event->id.'.jpg'])}}"><span class="glyphicon glyphicon-picture"></span></a></td>
                                <td>
                                    <div class="btn-group">
                                    <a class="btn btn-danger delete" data-id="{{$event->id}}" href="{{route('delete_event', ['id'=>$event->id])}}">Eliminar</a>
                                    <a class="btn btn-warning edit" data-id="{{$event->id}}" data-title="{{$event->title}}"
                                       data-description="{{$event->description}}" data-titlereference="{{$event->title_reference}}"
                                       data-reference="{{$event->reference}}" data-category="{{$event->category_id}}">Editar</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_image">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Imagen del Event</h4>
                </div>
                <div class="modal-body">
                    <img style="max-width: 100%;" id="img" src="#" alt="Imagen">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_evento">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Actualizar Evento</h4>
                </div>
                <div class="modal-body">
                    <form id="formulario" method="post" enctype="multipart/form-data" action="{{route('update_event')}}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="title_m">Título</label>
                                    <input type="text" min="4" max="50" maxlength="50" required name="title" id="title_m" class="form-control" placeholder="Título">
                                </div>
                                <div class="form-group">
                                    <label for="description_m">Descripción</label>
                                    <input type="text" min="4" max="1000" maxlength="1000" required name="description" id="description_m" placeholder="Descripción" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="title_reference_m">Título de Referencia</label>
                                    <input type="text" required min="4" max="50" name="title_reference" id="title_reference_m" class="form-control" placeholder="Título de referencia">
                                </div>
                                <div class="form-group">
                                    <label for="reference_m">Referencia</label>
                                    <input type="text" required min="4" max="50" name="reference" id="reference_m" placeholder="Referencia" class="form-control">
                                </div>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" required id="id" name="id">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="image_m">Imagen</label>
                                    <input type="file" name="image" id="image_m" class="form-control">
                                    <div id="d_image" class="error_info"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="category_id_m">Categoría</label>
                                    <select name="category_id" required id="category_id_m" class="form-control">
                                        <option value="">Seleccione...</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-md-offset-5 btn-group">
                                <button type="submit" class="btn btn-success" id="update">Guardar</button>
                                <a class="btn btn-danger" data-dismiss="modal">Cancelar</a>
                            </div>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
@endsection