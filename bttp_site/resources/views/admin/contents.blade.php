@extends('templates.admin')
@section('style')
    {!! Html::style('assets/css/bootstrap-select.css') !!}
    {!! Html::style('trumbowyg/ui/trumbowyg.min.css') !!}

@endsection
@section('script')
    {!! Html::script('assets/js/bootstrap-select.js') !!}
    <script>
        if (!$('#id').val()==''){
            $('#operation').val('MODIFY');
            $('#contents').css({
                'display': 'none'
            });
            $('.acept').css({
                'display': 'none'
            });
            $('.update').show();
            $('.cancel').show();
        }else{
            $('#operation').val('ADD');
            $('#contents').show();
            $('.acept').show();
            $('.update').css({
                'display': 'none'
            });
            $('.cancel').css({
                'display': 'none'
            });
        }

         var urlUpdate = '{{route('update_content')}}';
        var token='{{csrf_token()}}';
        $(document).ready(function () {
            $('#content').trumbowyg({
                semantic: false,
                autogrow: true
            });
            $('#sidebar').click(function() {
                if (!$(this).is(':checked')) {
                    $("#checksidebar").val('checked');
                }else{
                    $("#checksidebar").val('');
                }
            });
            $('#notice').click(function() {
                if (!$(this).is(':checked')) {
                    $("#checknotice").val('checked');
                }else{
                    $("#checknotice").val('');
                }
            });
            $(".selectpicker").selectpicker({
                noneSelectedText: 'Seleccione...'
            });
        });
        $(document).on("click", "#registrar", function (e) {
            e.preventDefault();
            serial = $("#register").serialize();
            console.log(serial);
            if (!confirm("¿Está seguro de registrar éste contenido?")){
                return;
            }

            $("#register").submit();
        });

        $(document).on("click", ".delete", function(e){
            if (!confirm('¿Está seguro de eliminar éste contenido?')){
                e.preventDefault();
                return;
            }
        });

        $(document).on("click", ".imagen", function(){
            img = $(this).data("img");
            $("#img").attr('src', img);
            $("#modal_image").modal();
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#imgprimary').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#image").change(function(){
            readURL(this);
        });
    </script>
    {!! Html::script('assets/js/adminContent.js') !!}
    {!! Html::script('trumbowyg/trumbowyg.min.js') !!}
@endsection
@section('content')
    @if(Session::has('mensaje'))
        <div class="alert @if(Session::has('error')) alert-danger @else alert-success @endif alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('mensaje')}}
        </div>
    @endif
    @if(Session::has('error'))
        {{Session::get('error')}}
    @endif
    <div class="panel panel-default">
        <div class="panel-heading">Administrar Contenidos</div>
        <div class="panel-body">
            <form id="register" action="{{route('create_content')}}" method="post" enctype="multipart/form-data">
                <h3>Registro</h3>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="title">Título</label>
                            <input type="text" name="title" id="title" class="form-control" placeholder="Título" value="{{old('title')}}">
                            @if ($errors->get('title'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('title') as $error)
                                        <strong>Error:</strong>{{$error}}<br>
                                    @endforeach
                                </div>
                            @endif
                            <input type="hidden" id="operation" name="operation"  value="ADD" >
                            <input type="hidden" id="id" name="id" value="{{old('id')}}"  >
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="description">Descripción</label>
                            <input type="text" name="description" id="description" class="form-control" placeholder="Descripción" value="{{old('description')}}">
                            @if ($errors->get('description'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('description') as $error)
                                        <strong>Error:</strong>{{$error}}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-6 col-sm-3">
                            <label for="image">Imagen Principal</label>
                            <input type="file" name="image" id="image" class="form-control" value="{{old('image')}}">
                            @if ($errors->get('image'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('image') as $error)
                                        <strong>Error:</strong>{{$error}}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <br>
                            <img style="max-width: 40%;" id="imgprimary" src="" alt="Imagen">
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-3">
                            <div class="form-group">
                                <label for="category_id">Categoría</label>
                                <select name="category_id" id="category_id" class="form-control">
                                    <option value="">Seleccione...</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}" @if(old('category_id')==$category->id) selected="selected" @endif>{{$category->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->get('category_id'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        @foreach($errors->get('category_id') as $error)
                                            <strong>Error:</strong>{{$error}}<br>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input name="sidebar" id="sidebar" {{old('checksidebar')}} type="checkbox"> Mostrar Sidebar?
                                        <input type="hidden" id="checksidebar" name="checksidebar" value=""  >
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="type">Tipo de Contenido</label>
                                <select name="type" id="type" class="form-control">
                                    <option value="" selected="selected">Seleccione...</option>
                                        <option value="0" @if(old('type')===0) selected="selected" @endif>Menú</option>
                                        <option value="1" @if(old('type')===1) selected="selected" @endif>Noticia</option>
                                        <option value="2" @if(old('type')===2) selected="selected" @endif>Pie de Página</option>
                                </select>
                                @if ($errors->get('type'))
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        @foreach($errors->get('type') as $error)
                                            <strong>Error:</strong>{{$error}}<br>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class="form-group">
                                <label for="tags">Etiquetas</label>
                                <select name="tags[]" id="tags" class="form-control selectpicker" data-live-search="true" multiple>
                                    @foreach($tags as $tag)
                                        <option value="{{$tag->id}}">{{$tag->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="description">Contenido</label>
                            <textarea name="content" id="content" class="form-control" rows="3">{{old('content')}}</textarea>
                            @if ($errors->get('content'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('content') as $error)
                                        <strong>Error:</strong>{{$error}}<br>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    <div class="col-md-3 col-md-offset-6">
                        <input type="submit" id="registrar" value="Registrar" class="btn btn-success acept">
                        <button type="submit" style="display: none;" onclick="$('#register').attr('action',urlUpdate);$('#operation').val('MODIFY');" class="btn btn-warning update">Modificar</button>
                        <a class="btn btn-danger cancel" style="display: none;"  >Cancelar</a>
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col-md-12">
                    <table id="contents" class="table table-responsive table-stripped table-hover" style="width: 100%;table-layout: fixed;">
                        <thead>
                        <tr>
                            <th style="width: 20px">#</th>
                            <th style="width: 10%;">Categoría</th>
                            <th>Título</th>
                            <th>Imagen</th>
                            <th>Muestra Sidebar</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contents as $content)
                            {{--*/
                                $contentTags = \App\ContentTag::where('content_id', '=', $content->id)->get();
                                $arrayTags = array();
                             /*--}}
                            @foreach($contentTags as $tagg)
                                {{--*/ array_push($arrayTags, $tagg->tag_id)/*--}}
                            @endforeach
                            <tr>
                                <td >{{$content->id}}</td>
                                <td style="word-break: break-all;">{{$content->category->name}}</td>
                                <td style=";width: 10%;">{{$content->title}}</td>
                                <td><a class="btn btn-primary imagen" data-img="{{route('content_image', ['filename' => 'Content-'.$content->id.'.jpg'])}}"><span class="glyphicon glyphicon-picture"></span></a></td>
                                <td style=";width: 5%;"> @if($content->sidebar==1)SI @else NO @endif</td>
                                <td style=";width: 5%;"> @if($content->type==0)Menú @else @if($content->type==1) Noticia @else Footer @endif @endif</td>
                                <td>
                                    <div class="btn-group">
                                    <a class="btn btn-danger delete" data-id="{{$content->id}}" href="{{route('delete_content', ['id'=>$content->id])}}">Eliminar</a>
                                    <a class="btn btn-warning edit" data-tags="{{json_encode($arrayTags)}}" data-img="{{route('content_image', ['filename' => 'Content-'.$content->id.'.jpg'])}}" data-id="{{$content->id}}" data-title="{{$content->title}}" data-contenttext="{{$content->content}}"
                                       data-category="{{$content->category_id}}" data-sidebar="{{$content->sidebar}}" data-description="{{$content->description}}" data-type="{{$content->type}}">Editar</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_image">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Imagen Principal</h4>
                </div>
                <div class="modal-body">
                    <img style="max-width: 100%;" id="img" src="#" alt="Imagen">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection