@extends('templates.admin')
@section('script')
    <script>
        var urlUpdate = '{{route('update_user')}}';
        var token='{{csrf_token()}}';
        $(document).ready(function () {
        });
        $(document).on("click", "#registrar", function (e) {
            e.preventDefault();
            if (!confirm("¿Está seguro de registrar éste usuario?")){
                return;
            }
            $("form").submit();
        });

        $(document).on("click", ".delete", function(e){
            if (!confirm('¿Está seguro de eliminar éste usuario?')){
                e.preventDefault();
                return;
            }
        });
    </script>
    {!! Html::script('assets/js/adminUser.js') !!}
@endsection
@section('content')
    @if(Session::has('mensaje'))
        <div class="alert @if(Session::has('error')) alert-danger @else alert-success @endif alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('mensaje')}}
        </div>
    @endif
    <div class="panel panel-default">
        <div class="panel-heading">Administrar Usuarios</div>
        <div class="panel-body">
            <form action="{{route('create_user')}}" method="post">
                <h3>Registro</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">Correo</label>
                            <input type="text" name="email" id="email" class="form-control" placeholder="Correo" value="{{old('email')}}">
                            @if ($errors->get('email'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('email') as $error)
                                        <strong>Error:</strong>{{$error}}
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password">Clave</label>
                            <input type="password" name="password" id="password" class="form-control"
                                   placeholder="Clave">
                            @if ($errors->get('password'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('password') as $error)
                                        <strong>Error:</strong>{{$error}}
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Confirmación</label>
                            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control"
                                   placeholder="Clave">
                        </div>
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nombre">Nombre</label>
                            <input type="text" name="first_name" id="nommbre" placeholder="Nombre" class="form-control" value="{{old('first_name')}}">
                            @if ($errors->get('first_name'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('first_name') as $error)
                                        <strong>Error:</strong>{{$error}}
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="apellido">Apellido</label>
                            <input type="text" name="last_name" id="apellido" placeholder="Apellido" class="form-control" value="{{old('last_name')}}">
                            @if ($errors->get('last_name'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('last_name') as $error)
                                        <strong>Error:</strong>{{$error}}
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="tipo_usuario_id">Tipo Usuario</label>
                            <select name="role_id" id="tipo_usuario_id" class="form-control">
                                <option value="">Seleccione...</option>
                                @foreach($roles as $role)
                                    <option value="{{$role->id}}" @if(old('role_id')==$role->id) selected="selected" @endif>{{$role->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->get('role_id'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('role_id') as $error)
                                        <strong>Error:</strong>{{$error}}
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-md-offset-6">
                        <input type="submit" id="registrar" value="Registrar" class="btn btn-success">
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <table id="asignaturas" class="table table-responsive table-stripped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Correo</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->id}}</td>
                                <td>{{$user->first_name}}</td>
                                <td>{{$user->last_name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->role->name}}</td>
                                <td>
                                    <a class="btn btn-danger delete" data-id="{{$user->id}}" href="{{route('delete_user', ['id'=>$user->id])}}">Eliminar</a>
                                    <a class="btn btn-warning edit" data-id="{{$user->id}}" data-firstname="{{$user->first_name}}"
                                       data-lastname="{{$user->last_name}}" data-email="{{$user->email}}" data-role="{{$user->role_id}}">Editar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_user">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Imagen del Slider</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="email_m">Correo</label>
                                <input type="text" id="email_m" class="form-control" placeholder="Correo" value="{{old('email')}}">
                                <div id="d_email" class="error_info"></div>
                            </div>
                            <div class="form-group">
                                <label for="first_name_m">Nombre</label>
                                <input type="text" id="fist_name_m" placeholder="Nombre" class="form-control" value="{{old('first_name')}}">
                                <div id="d_first_name" class="error_info"></div>
                            </div>
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="last_name_m">Apellido</label>
                                <input type="text" id="last_name_m" placeholder="Apellido" class="form-control">
                                <div id="d_last_name" class="error_info"></div>
                            </div>
                            <div class="form-group">
                                <label for="role_id_m">Tipo Usuario</label>
                                <select id="role_id_m" class="form-control">
                                    <option value="">Seleccione...</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                                <div id="d_role_id" class="error_info"></div>
                            </div>
                            <input type="hidden" id="id">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-md-offset-5">
                            <a class="btn btn-success" id="update">Guardar</a>
                            <a class="btn btn-danger" data-dismiss="modal">Cancelar</a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection