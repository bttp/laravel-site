@extends('templates.admin')
@section('script')
    <script>
        var urlUpdate = '{{route('update_category')}}';
        var token='{{csrf_token()}}';
        $(document).ready(function () {
        });
        $(document).on("click", "#registrar", function (e) {
            e.preventDefault();
            if (!confirm("¿Está seguro de registrar ésta categoría?")){
                return;
            }
            $("form").submit();
        });

        $(document).on("click", ".delete", function(e){
            if (!confirm('¿Está seguro de eliminar ésta categoría?')){
                e.preventDefault();
                return;
            }
        });
    </script>
    {!! Html::script('assets/js/adminCategory.js') !!}
@endsection
@section('content')
    @if(Session::has('mensaje'))
        <div class="alert @if(Session::has('error')) alert-danger @else alert-success @endif alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('mensaje')}}
        </div>
    @endif
    <div class="panel panel-default">
        <div class="panel-heading">Administrar Categorías</div>
        <div class="panel-body">
            <form action="{{route('create_category')}}" method="post">
                <h3>Registro</h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Nombre</label>
                            <input type="text" name="name" id="name" class="form-control" placeholder="Nombre" value="{{old('name')}}">
                            @if ($errors->get('name'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('name') as $error)
                                        <strong>Error:</strong>{{$error}}
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="description">Descripción</label>
                            <input type="text" name="description" id="description" class="form-control"
                                   placeholder="Descripción">
                            @if ($errors->get('description'))
                                <div class="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->get('description') as $error)
                                        <strong>Error:</strong>{{$error}}
                                    @endforeach
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="row">
                    <div class="col-md-3 col-md-offset-6">
                        <input type="submit" id="registrar" value="Registrar" class="btn btn-success">
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <table id="asignaturas" class="table table-responsive table-stripped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Descriptión</th>
                            <th>Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{$category->id}}</td>
                                <td>{{$category->name}}</td>
                                <td>{{$category->description}}</td>
                                <td>
                                    <div class="btn-group">
                                    <a class="btn btn-danger delete" data-id="{{$category->id}}" href="{{route('delete_category', ['id'=>$category->id])}}">Eliminar</a>
                                    <a class="btn btn-warning edit" data-id="{{$category->id}}" data-name="{{$category->name}}"
                                       data-description="{{$category->description}}">Editar</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="modal_category">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Actualizar Categoría</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name_m">Nombre</label>
                                <input type="text" id="name_m" class="form-control" placeholder="Correo">
                                <div id="d_name" class="error_info"></div>
                            </div>
                            <div class="form-group">
                                <label for="description_m">Descripción</label>
                                <input type="text" id="description_m" placeholder="Descripción" class="form-control">
                                <div id="d_description" class="error_info"></div>
                            </div>
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" id="id">
                        </div>
                    <div class="row">
                        <div class="col-md-3 col-md-offset-5 btn-group">
                            <a class="btn btn-success" id="update">Guardar</a>
                            <a class="btn btn-danger" data-dismiss="modal">Cancelar</a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection