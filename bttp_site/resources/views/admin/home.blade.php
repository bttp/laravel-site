@extends('templates.admin')
@section('content')
    @if(Session::has('mensaje'))
        <div class="alert @if(Session::has('error')) alert-danger @else alert-success @endif alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('mensaje')}}
        </div>
    @endif
    <div class="container">
        <h1 align="center">Administración de Contenidos</h1>
    </div>
@endsection