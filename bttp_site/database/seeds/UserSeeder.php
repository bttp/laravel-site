<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $users = [
            [
                'email' => 'admin@site.com',
                'password' => bcrypt('123123'),
                'first_name' => 'Admin',
                'last_name' => 'Site',
                'role_id' => 1,
            ],
            [
                'email' => 'editor@site.com',
                'password' => bcrypt('123123'),
                'first_name' => 'Editor',
                'last_name' => 'Site',
                'role_id' => 2,
            ]
        ];
        foreach ($users as $user) {
            \App\User::create($user);
        }
    }
}
