<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roles = [
            [
                'name' => 'Admin',
                'description' => 'Usuarios administradores'
            ],
            [
                'name' => 'Editor',
                'description' => 'Usuarios Editores'
            ]
        ];
        foreach ($roles as $role) {
            \App\Role::create($role);
        }
    }
}
