$(document).on('click', '.edit', function(){
    $(".error_info").html("");
    first_name = $(this).data('firstname');
    last_name = $(this).data('lastname');
    email = $(this).data('email');
    id = $(this).data('id');
    role_id = $(this).data('role');
    $("#fist_name_m").val(first_name);
    $("#last_name_m").val(last_name);
    $("#email_m").val(email);
    $("#role_id_m").val(role_id);
    $("#id").val(id);
    $("#modal_user").modal();
});

$(document).on("click", "#update", function(){
    first_name = $("#fist_name_m").val();
    last_name = $("#last_name_m").val();
    email = $("#email_m").val();
    role_id = $("#role_id_m").val();
    id = $("#id").val();
    $.ajax({
        method:'post',
        url:urlUpdate,
        dataType:'json',
        data:{id:id,first_name:first_name,last_name:last_name,email:email,role_id:role_id,_token:token}
    }).done(function (res) {
        console.log(res);
        alert(res.mensaje);
        location.reload();
    }).error(function (msj) {
        $.each(msj.responseJSON, function(k, v){
            htmlError = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Error:</strong>'+v+'</div>';
            $("#d_"+k).html(htmlError);
        });
    });
});