$(document).on('click', '.edit', function(){
    $(".error_info").html("");
    name = $(this).data('name');
    description = $(this).data('description');
    id = $(this).data('id');
    $("#name_m").val(name);
    $("#description_m").val(description);
    $("#id").val(id);
    $("#modal_category").modal();
});

$(document).on("click", "#update", function(){
    name = $("#name_m").val();
    description = $("#description_m").val();
    id = $("#id").val();
    $.ajax({
        method:'post',
        url:urlUpdate,
        dataType:'json',
        data:{id:id,name:name,description:description,_token:token}
    }).done(function (res) {
        console.log(res);
        alert(res.mensaje);
        location.reload();
    }).error(function (msj) {
        console.log(msj);
        $.each(msj.responseJSON, function(k, v){
            htmlError = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> <strong>Error:</strong>'+v+'</div>';
            $("#d_"+k).html(htmlError);
        });
    });
});