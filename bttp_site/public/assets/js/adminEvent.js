$(document).on('click', '.edit', function(){
    $(".error_info").html("");
    title = $(this).data('title');
    description = $(this).data('description');
    title_reference = $(this).data('titlereference');
    reference = $(this).data('reference');
    category_id = $(this).data('category');
    id = $(this).data('id');
    $("#title_m").val(title);
    $("#description_m").val(description);
    $("#title_reference_m").val(title_reference);
    $("#reference_m").val(reference);
    $("#category_id_m").val(category_id);
    $("#id").val(id);
    $("#modal_evento").modal();
});

$(document).on("click", "#update", function(e){
    if(!confirm('¿Está segur@ de modificar éste registro?')){
        e.preventDefault();
    }
});