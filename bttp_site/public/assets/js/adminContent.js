$(document).on('click', '.edit', function () {
    data = $(this).data('tags');
    $("#tags").val(data);
    $("#tags").selectpicker('refresh');
    $(".error_info").html("");
    id = $(this).data("id");
    $("#title").val($(this).data('title'));
    $("#description").val($(this).data('description'));
    $("#category_id").val($(this).data('category'));
    $('#content').trumbowyg('html', $(this).data('contenttext'));
    $('#imgprimary').attr('src', $(this).data('img'))
    sidebar = $(this).data('sidebar');
    $("#type").val($(this).data('type'));
    if (sidebar) {
        $("#sidebar").attr('checked', 'checked');
        $("#checksidebar").val('checked');
    } else {
        $("#checksidebar").val('');
    }
    

    //---------------
    modify(id);
});

$(document).on("click", "#update", function (e) {
    if (!confirm('¿Está segur@ de modificar éste registro?')) {
        e.preventDefault();
    }
});
$(document).on("click", ".cancel", function (e) {
    $("#tags").val([]);
    $("#tags").selectpicker('refresh');
    $("#title").val('');
    $("#description").val('');
    $("#imgprimary").attr('src', '');
    $("#category_id").val('');
    $("#type").val('');
    $('#content').trumbowyg('html', '');
    sidebar = $(this).data('sidebar');
    $("#checksidebar").val('');
    $("#sidebar").attr('checked', '');
    $('#contents').show();
    $('.acept').show();
    $('.update').css({
        'display': 'none'
    });
    $('.cancel').css({
        'display': 'none'
    });
});


function modify(id) {
    $("#operation").val('MODIFY');
    $("#id").val(id);
    $('#contents').css({
        'display': 'none'
    });
    $('.acept').css({
        'display': 'none'
    });
    $('.update').show();
    $('.cancel').show();
}
