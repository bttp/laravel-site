<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContentTag extends Model
{
    //
    public function content()
    {
        return $this->belongsTo('\App\Content');
    }

    public function tag()
    {
        return $this->belongsTo('\App\Tag');
    }
}
