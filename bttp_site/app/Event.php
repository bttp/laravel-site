<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    protected $fillable = [
        'title', 'description', 'title_reference', 'reference', 'category_id', 'created_by'
    ];

    public function category()
    {
        return $this->belongsTo('\App\Category');
    }
}
