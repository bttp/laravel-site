<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
    'uses' => 'WebController@index',
    'as' => 'home'
]);

Route::get('/page/{id?}/{page?}', [
    'uses' => 'WebController@page',
    'as' => 'page'
]);

/**
 * Rutas para CMS
 */

Route::get('/admin', [
    'uses' => 'AuthController@getLogin',
    'as' => 'get_login'
]);

Route::post('/login', [
    'uses' => 'AuthController@postLogin',
    'as' => 'login'
]);

Route::get('/logout', [
    'uses' => 'AuthController@getLogout',
    'as' => 'logout'
]);

Route::get('/admin/home', function(){
    return view('admin.home');
})->name('home_admin');


Route::get('/test_admin', [
    'uses' => 'UserController@test',
    'as' => 'test_admin',
    'middleware' => ['auth', 'admin']
]);

Route::get('/admin/users', [
    'uses' => 'UserController@getUsers',
    'as' => 'admin_users',
    'middleware' => ['auth', 'admin']
]);

Route::post('/user/create', [
    'uses' => 'UserController@postCreateUser',
    'as' => 'create_user',
    'middleware' => ['auth', 'admin']
]);

Route::post('/user/update', [
    'uses' => 'UserController@postUpdateUser',
    'as' => 'update_user',
    'middleware' => ['auth', 'admin']
]);

Route::get('/user/delete/{id}', [
    'uses' => 'UserController@getDeleteUser',
    'as' => 'delete_user',
    'middleware' => ['auth', 'admin']
]);

Route::get('/admin/categories', [
    'uses' => 'CategoryController@getCategories',
    'as' => 'admin_categories',
    'middleware' => ['auth', 'editor']
]);

Route::post('/category/create', [
    'uses' => 'CategoryController@postCreateCategory',
    'as' => 'create_category',
    'middleware' => ['auth', 'editor']
]);

Route::post('/category/update', [
    'uses' => 'CategoryController@postUpdateCategory',
    'as' => 'update_category',
    'middleware' => ['auth', 'editor']
]);

Route::get('/category/delete/{id}', [
    'uses' => 'CategoryController@getDeleteCategory',
    'as' => 'delete_category',
    'middleware' => ['auth', 'editor']
]);

Route::get('/admin/tags', [
    'uses' => 'TagController@getTags',
    'as' => 'admin_tags',
    'middleware' => ['auth', 'editor']
]);

Route::post('/tag/create', [
    'uses' => 'TagController@postCreateTag',
    'as' => 'create_tag',
    'middleware' => ['auth', 'editor']
]);

Route::post('/tag/update', [
    'uses' => 'TagController@postUpdateTag',
    'as' => 'update_tag',
    'middleware' => ['auth', 'editor']
]);

Route::get('/tag/delete/{id}', [
    'uses' => 'TagController@getDeleteTag',
    'as' => 'delete_tag',
    'middleware' => ['auth', 'editor']
]);

Route::get('/admin/sliders', [
    'uses' => 'SliderController@getSliders',
    'as' => 'admin_sliders',
    'middleware' => ['auth', 'editor']
]);

Route::get('/slider/image/{filename}', [
    'uses' => 'SliderController@getSliderImage',
    'as' => 'slider_image',
    'middleware' => ['auth', 'editor']
]);

Route::post('/slider/create', [
    'uses' => 'SliderController@postCreateSlider',
    'as' => 'create_slider',
    'middleware' => ['auth', 'editor']
]);

Route::post('/sliders/update', [
    'uses' => 'SliderController@postUpdateSlider',
    'as' => 'update_slider',
    'middleware' => ['auth', 'editor']
]);

Route::get('/slider/delete/{id}', [
    'uses' => 'SliderController@getDeleteSlider',
    'as' => 'delete_slider',
    'middleware' => ['auth', 'editor']
]);


Route::get('/admin/events', [
    'uses' => 'EventController@getEvents',
    'as' => 'admin_events',
    'middleware' => ['auth', 'editor']
]);

Route::get('/event/image/{filename}', [
    'uses' => 'EventController@getEventImage',
    'as' => 'event_image',
    'middleware' => ['auth', 'editor']
]);

Route::post('/event/create', [
    'uses' => 'EventController@postCreateEvent',
    'as' => 'create_event',
    'middleware' => ['auth', 'editor']
]);

Route::post('/event/update', [
    'uses' => 'EventController@postUpdateEvent',
    'as' => 'update_event',
    'middleware' => ['auth', 'editor']
]);

Route::get('/event/delete/{id}', [
    'uses' => 'EventController@getDeleteEvent',
    'as' => 'delete_event',
    'middleware' => ['auth', 'editor']
]);

/**
 * Content Edit
 */

Route::get('/admin/contents', [
    'uses' => 'ContentController@getContents',
    'as' => 'admin_contents',
    'middleware' => ['auth', 'editor']
]);

Route::get('/content/image/{filename}', [
    'uses' => 'ContentController@getContentImage',
    'as' => 'content_image',
    'middleware' => ['auth', 'editor']
]);

Route::post('/content/create', [
    'uses' => 'ContentController@postCreateContent',
    'as' => 'create_content',
    'middleware' => ['auth', 'editor']
]);

Route::post('/content/update', [
    'uses' => 'ContentController@postUpdateContent',
    'as' => 'update_content',
    'middleware' => ['auth', 'editor']
]);

Route::get('/content/delete/{id}', [
    'uses' => 'ContentController@getDeleteContent',
    'as' => 'delete_content',
    'middleware' => ['auth', 'editor']
]);