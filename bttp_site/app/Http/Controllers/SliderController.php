<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    //
    public function getSliders()
    {
        $sliders = Slider::all();
        return view('admin.sliders')->with(['sliders'=>$sliders]);
    }

    public function postCreateSlider(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:sliders|min:4|max:50',
            'description' => 'required|min:4|max:100',
            'title_reference' => 'required|min:4|max:50',
            'reference' => 'required|min:4|max:100',
            'image' => 'required|file|image|mimes:jpg,jpeg|min:200|max:2024'
        ]);
        try{
            $slider = Slider::create([
                'title'=>$request["title"],
                'description'=>$request["description"],
                'title_reference'=>$request["title_reference"],
                'reference'=>$request["reference"],
                'created_by'=>Auth::user()->id
            ]);
        }catch (\Exception $e){
            return redirect()->back()->with(['mensaje'=>'Error al crear éste registro!', 'code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        $file=$request["image"];
        $filename="Slider-".$slider->id.".jpg";
        if($file){
            Storage::disk('local')->put($filename,File::get($file));
            $request->file('image')->move(
                base_path().'/public/img/upload/slider/',$filename
            );
        }
        return redirect()->back()->with(['mensaje' => 'Registro exitoso!', 'slider' => $slider]);
    }

    public function postUpdateSlider(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:4|max:50|unique:sliders,id,'.$request["id"],
            'description' => 'required|min:4|max:100',
            'title_reference' => 'required|min:4|max:50',
            'reference' => 'required|min:4|max:100',
            'image' => 'file|image|mimes:jpg,jpeg|min:200|max:2024',
        ]);
        if (!$slider = Slider::find($request["id"])){
            return response()->json(['mensaje' => 'No se consiguió slider!']);
        }
        $slider->title = $request["title"];
        $slider->description = $request["description"];
        $slider->title_reference = $request["title_reference"];
        $slider->reference = $request["reference"];
        try{
            $slider->update();
        }catch (\Exception $e){
            return redirect()->back()->with(['mensaje'=>'Error al actualizar éste registro!', 'code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        if($request["image"]){
            $file=$request["image"];
            $filename="Slider-".$slider->id.".jpg";
            Storage::disk('local')->put($filename,File::get($file));
            $request->file('image')->move(
                base_path().'/public/img/upload/slider/',$filename
            );
        }
        return redirect()->back()->with(['mensaje' => 'Actualización exitosa!', 'slider' => $slider]);
    }

    public function getDeleteSlider($id)
    {
        if (!$slider = Slider::find($id)){
            return redirect()->back()->with(['mensaje' => 'No se consiguió slider!']);
        }
        if ($slider->created_by!=Auth::user()->id){
            return redirect()->back()->with(['mensaje' => 'No puede eliminar contenido que no ha creado ud!', 'error'=>'No es dueño del registro']);
        }
        try{
            $slider->delete();
        }catch (\Exception $e){
            return redirect()->back()->with(['mensaje'=>'Error al eliminar éste registro!', 'code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        return redirect()->back()->with(['mensaje'=>'Eliminación exitosa!', 'slider'=>$slider]);
    }

    public function getSliderImage($filename)
    {
        $imagen=Storage::disk('local')->get($filename);
        return new Response($imagen, 200);
    }
}
