<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    //
    public function getCategories()
    {
        $categories = Category::all();
        return view('admin.categories')->with(['categories'=>$categories]);
    }

    public function postCreateCategory(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:categories|min:3|max:50',
            'description' => 'required|min:3|max:100'
        ]);
        try{
            $category = Category::create(array('name'=>$request["name"], 'description'=>$request["description"], 'created_by'=>Auth::user()->id));
        }catch (\Exception $e){
            return redirect()->back()->with(['mensaje'=>'Error al crear éste registro!', 'code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        return redirect()->back()->with(['mensaje' => 'Registro exitoso!', 'category' => $category]);
    }

    public function postUpdateCategory(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:50|unique:categories,id,'.$request["id"],
            'description' => 'required|min:3|max:100',
        ]);
        if (!$category = Category::find($request["id"])){
            return response()->json(['mensaje' => 'No se consiguió categoría!']);
        }
        $category->name = $request["name"];
        $category->description = $request["description"];
        try{
            $category->update();
        }catch (\Exception $e){
            return response()->json(['mensaje'=>'Error al actualizar éste registro!', 'code'=>$e->getCode(), 'error'=>$e->getMessage()], $e->getCode());
        }
        return response()->json(['mensaje' => 'Actualización exitosa!', 'category' => $category]);
    }

    public function getDeleteCategory($id)
    {
        if (!$category = Category::find($id)){
            return redirect()->back()->with(['mensaje' => 'No se consiguió categoría!']);
        }
        if ($category->created_by!=Auth::user()->id){
            return redirect()->back()->with(['mensaje' => 'No puede eliminar contenido que no ha creado ud!', 'error'=>'No es dueño del registro']);
        }
        try{
            $category->delete();
        }catch (\Exception $e){
            return redirect()->back()->with(['mensaje'=>'Error al eliminar éste registro!', 'code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        return redirect()->back()->with(['mensaje'=>'Eliminación exitosa!', 'category'=>$category]);
    }
}
