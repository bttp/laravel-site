<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function getLogin()
    {
        if (Auth::check()){
            return redirect()->route('home_admin');
        }
        return view('admin.index');
    }

    public function postLogin (Request $request)
    {
        $this->validate($request, [
            'email'=>'required',
            'password' => 'required'
        ]);
        if (Auth::attempt(['email' => $request["email"], 'password' => $request["password"]])) {
            return redirect()->route('home_admin');
        }
        session()->flash('mensaje', 'Credenciales Inválidas!');
        return redirect()->back()->with(['mensaje'=>'Credenciale inválidas']);
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('get_login');
    }
}
