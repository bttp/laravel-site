<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class UserController extends Controller
{
    public function test()
    {
        return 1;
    }

    public function getUsers()
    {
        $users = User::all();
        $roles = Role::all();
        return view('admin.users')->with(['users'=>$users, 'roles'=>$roles]);
    }

    public function postCreateUser(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:users|email|min:4|max:62',
            'password' => 'required|min:6|max:20|confirmed',
            'first_name' => 'required|min:3|max:50',
            'last_name' => 'required|min:3|max:50',
            'role_id' => 'required|exists:roles,id',
        ]);
        $request["password"] = bcrypt($request["password"]);
        try{
            $user = User::create($request->only([
                'email',
                'password',
                'first_name',
                'last_name',
                'role_id',
            ]));
        }catch (\Exception $e){
            return redirect()->back()->with(['mensaje'=>'Error al crear éste registro!', 'code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        return redirect()->back()->with(['mensaje' => 'Registro exitoso!', 'user' => $user]);
    }

    public function postUpdateUser(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|min:4|max:62|unique:users,id,'.$request["id"],
            'first_name' => 'required|min:3|max:50',
            'last_name' => 'required|min:3|max:50',
            'role_id' => 'required|exists:roles,id'
        ]);
        if (!$user = User::find($request["id"])){
            return response()->json(['error' => 'No se consiguió usuario!']);
        }
        $user->email = $request["email"];
        $user->password = bcrypt($request["password"]);
        $user['first_name'] = $request["first_name"];
        $user['last_name'] = $request["last_name"];
        $user->role_id = $request["role_id"];
        try{
            $user->update();
        }catch (\Exception $e){
            return redirect()->back()->with(['mensaje'=>'Error al actualizar éste registro!', 'code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        return response()->json(['mensaje' => 'Actualización exitosa!', 'user' => $user]);
    }

    public function getDeleteUser($id)
    {
        if (!$user = User::find($id)){
            return redirect()->back()->with(['mensaje' => 'No se consiguió usuario!']);
        }
        try{
            $user->delete();
        }catch (\Exception $e){
            return redirect()->back()->with(['mensaje'=>'Error al eliminar éste registro!', 'code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        return redirect()->back()->with(['mensaje'=>'Eliminación exitosa!', 'user'=>$user]);
    }
}
