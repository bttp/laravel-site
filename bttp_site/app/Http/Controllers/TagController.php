<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{
    //
    public function getTags()
    {
        $tags = Tag::all();
        return view('admin.tags')->with(['tags'=>$tags]);
    }

    public function postCreateTag(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:tags|min:3|max:50',
            'description' => 'required|min:3|max:100'
        ]);
        try{
            $tag = Tag::create([
                'name'=>$request["name"],
                'description'=>$request["description"],
                'created_by'=>Auth::user()->id
            ]);
        }catch (\Exception $e){
            return redirect()->back()->with(['mensaje'=>'Error al crear éste registro!', 'code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        return redirect()->back()->with(['mensaje' => 'Registro exitoso!', 'tag' => $tag]);
    }

    public function postUpdateTag(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:50|unique:tags,id,'.$request["id"],
            'description' => 'required|min:3|max:100',
        ]);
        if (!$tag = Tag::find($request["id"])){
            return response()->json(['mensaje' => 'No se consiguió etiqueta!']);
        }
        $tag->name = $request["name"];
        $tag->description = $request["description"];
        try{
            $tag->update();
        }catch (\Exception $e){
            return redirect()->back()->with(['mensaje'=>'Error al actualizar éste registro!', 'code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        return response()->json(['mensaje' => 'Actualización exitosa!', 'tag' => $tag]);
    }

    public function getDeleteTag($id)
    {
        if (!$tag = Tag::find($id)){
            return redirect()->back()->with(['mensaje' => 'No se consiguió etiqueta!']);
        }
        if ($tag->created_by!=Auth::user()->id){
            return redirect()->back()->with(['mensaje' => 'No puede eliminar contenido que no ha creado ud!', 'error'=>'No es dueño del registro']);
        }
        try{
            $tag->delete();
        }catch (\Exception $e){
            return redirect()->back()->with(['mensaje'=>'Error al eliminar éste registro!', 'code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        return redirect()->back()->with(['mensaje'=>'Eliminación exitosa!', 'tag'=>$tag]);
    }
}
