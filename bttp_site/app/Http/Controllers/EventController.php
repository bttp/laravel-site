<?php

namespace App\Http\Controllers;


use App\Category;
use App\Event;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class EventController extends Controller
{
    //
    public function getEvents()
    {
        $events = Event::all();
        $categories = Category::all();
        return view('admin.events')->with(['events'=>$events, 'categories' => $categories]);
    }

    public function postCreateEvent(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:events|min:4|max:50',
            'description' => 'required|min:10|max:1000',
            'title_reference' => 'required|min:4|max:50',
            'reference' => 'required|min:4|max:200',
            'category_id' => 'required|exists:categories,id',
            'image' => 'required|file|image|mimes:jpeg,jpg|min:20|max:2024'
        ]);
        try{
            $event = Event::create([
                'title'=>$request["title"],
                'description'=>$request["description"],
                'title_reference'=>$request["title_reference"],
                'reference'=>$request["reference"],
                'category_id'=>$request["category_id"],
                'created_by'=>Auth::user()->id
            ]);
        }catch (\Exception $e){
            return redirect()->back()->with(['mensaje'=>'Error al crear éste registro!', 'code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        $file=$request["image"];
        $filename="Event-".$event->id.".jpg";
        if($file){
            Storage::disk('local')->put($filename,File::get($file));
            $request->file('image')->move(
                base_path().'/public/img/upload/event/',$filename
            );
        }
        return redirect()->back()->with(['mensaje' => 'Registro exitoso!', 'event' => $event]);
    }

    public function postUpdateEvent(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:4|max:50|unique:categories,id,'.$request["id"],
            'description' => 'required|min:4|max:1000',
            'title_reference' => 'required|min:4|max:50',
            'reference' => 'required|min:4|max:200',
            'category_id' => 'required|exists:categories,id',
            'image' => 'file|image|mimes:jpeg,jpg|min:20|max:2024'
        ]);
        if (!$event = Event::find($request["id"])){
            return redirect()->back()->with(['mensaje' => 'No se consiguió evento!']);
        }
        $event->title = $request["title"];
        $event->description = $request["description"];
        $event->title_reference = $request["title_reference"];
        $event->reference = $request["reference"];
        $event->category_id = $request["category_id"];
        if($request["image"]){
            $file=$request["image"];
            $filename="Event-".$event->id.".jpg";
            Storage::disk('local')->put($filename,File::get($file));
            $request->file('image')->move(
                base_path().'/public/img/upload/event/',$filename
            );
        }
        try{
            $event->update();
        }catch (\Exception $e){
            return redirect()->back()->with(['mensaje'=>'Error al actualizar éste registro!', 'code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        return redirect()->back()->with(['mensaje' => 'Actualización exitosa!', 'event' => $event]);
    }

    public function getDeleteEvent($id)
    {
        if (!$event = Event::find($id)){
            return redirect()->back()->with(['mensaje' => 'No se consiguió evento!']);
        }
        if ($event->created_by!=Auth::user()->id){
            return redirect()->back()->with(['mensaje' => 'No puede eliminar contenido que no ha creado ud!', 'error'=>'No es dueño del registro']);
        }
        try{
            $event->delete();
        }catch (\Exception $e){
            return redirect()->back()->with(['mensaje'=>'Error al eliminar éste registro!', 'code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        return redirect()->back()->with(['mensaje'=>'Eliminación exitosa!', 'event'=>$event]);
    }

    public function getEventImage($filename)
    {
        $imagen=Storage::disk('local')->get($filename);
        return new Response($imagen, 200);
    }
}
