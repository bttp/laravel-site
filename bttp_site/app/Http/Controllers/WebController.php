<?php

namespace App\Http\Controllers;

use App\Content;
use Illuminate\Http\Request;

use App\Http\Requests;

class WebController extends Controller
{
    public function index()
    {
        $class='active';
        $sliders = \App\Slider::all();
        $categories = \App\Category::all();
        $events = \App\Event::all();
        $contents = \App\Content::orderby('created_at', 'desc')->get();
        $dirimg='';
        $content='';
        $page='home';
        return view('index')->with(['sliders'=>$sliders, 'categories'=>$categories, 'events'=>$events,'class'=>$class, 'contents'=>$contents,'dirimg'=>$dirimg,'content'=>$content,'page'=>$page]);
    }

    public function page($id,$page)
    {
        if(!$content = Content::find($id)){
            return redirect()->back()->with(['mensaje' => 'No se consiguió la página!']);
        }
        $class='';
        $dirimg='../../';
        $contents = \App\Content::orderby('created_at', 'desc')->get();
        return view('page')->with(['class'=>$class, 'content'=>$content,'page'=>$page,'contents'=>$contents,'dirimg'=>$dirimg]);
    }
}
