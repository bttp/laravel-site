<?php


namespace App\Http\Controllers;


use App\Category;
use App\Content;
use App\ContentTag;
use App\Tag;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use DB;

class ContentController extends Controller
{
    //
    public function getContents()
    {
        $contents = Content::all();
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.contents')->with(['contents' => $contents, 'categories' => $categories, 'tags' => $tags]);
    }

    public function postCreateContent(Request $request)
    {
        $request["sidebar"] = ($request["sidebar"] == "on") ? 1 : 0;
        $this->validate($request, [
            'sidebar' => 'required|boolean',
            'title' => 'unique:contents|min:4|max:100',
            'description' => 'min:4|max:100',
            'type' => 'required',
            'category_id' => 'required|exists:categories,id'
        ]);
        DB::beginTransaction();
        try{
            $content = Content::create([
                'title'=>$request["title"],
                'description'=>$request["description"],
                'content'=>$request["content"],
                'category_id'=>$request["category_id"],
                'sidebar'=>$request["sidebar"],
                'type'=>$request["type"],
                'created_by'=>Auth::user()->id
            ]);
            foreach($_REQUEST["tags"] as $tag_r){
                $tag = new ContentTag();
                $tag->content_id = $content->id;
                $tag->tag_id = $tag_r;
                $tag->save();
            }
        }catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->with(['mensaje' => 'Ocurrió un inconveniente en la transacción de datos!','code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        DB::commit();
        $file = $request["image"];
        $filename = "Content-" . $content->id . ".jpg";
        if ($file) {
            Storage::disk('local')->put($filename, File::get($file));
            $request->file('image')->move(
                base_path() . '/public/img/upload/content/', $filename
            );
        }
        return redirect()->back()->with(['mensaje' => 'Registro exitoso!', 'content' => $content]);

    }

    public function postUpdateContent(Request $request)
    {
        $request["sidebar"] = ($request["sidebar"] == "on") ? 1 : 0;
        $this->validate($request, [
            'sidebar' => 'required|boolean',
            'title' => 'min:4|max:100|unique:contents,id,'.$request["id"],
            'description' => 'min:4|max:100',
            'type' => 'required',
            'category_id' => 'required|exists:categories,id'
        ]);
        if (!$content = Content::find($request["id"])) {
            return redirect()->back()->with(['mensaje' => 'No se consiguió el Contenido!']);
        }
        $content->title = $request["title"];
        $content->description = $request["description"];
        $content->content = $request["content"];
        $content->sidebar = $request["sidebar"];
        $content->type = $request["type"];
        $content->category_id = $request["category_id"];
        if ($request["image"]) {
            $file = $request["image"];
            $filename = "Content-" . $content->id . ".jpg";
            Storage::disk('local')->put($filename, File::get($file));
            $request->file('image')->move(
                base_path() . '/public/img/upload/content/', $filename
            );
        }
        DB::beginTransaction();
        try{
            DB::table('content_tags')->where('content_id', '=', $content->id)->delete();
            $content->update();
            foreach($_REQUEST["tags"] as $tag_r){
                $tag = new ContentTag();
                $tag->content_id = $content->id;
                $tag->tag_id = $tag_r;
                $tag->save();
            }
        }catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->with(['mensaje' => 'Ocurrió un inconveniente en la transacción de datos!','code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        DB::commit();
        return redirect()->back()->with(['mensaje' => 'Actualización exitosa!', 'content' => $content]);
    }

    public function getDeleteContent($id)
    {
        if (!$content = Content::find($id)) {
            return redirect()->back()->with(['mensaje' => 'No se consiguió el Contenido!']);
        }
        if ($content->created_by!=Auth::user()->id){
            return redirect()->back()->with(['mensaje' => 'No puede eliminar contenido que no ha creado ud!', 'error'=>'No es dueño del registro']);
        }
        DB::beginTransaction();
        try{
            DB::table('content_tags')->where('content_id', '=', $id)->delete();
            $content->delete();
        }catch (\Exception $e){
            DB::rollBack();
            return redirect()->back()->with(['mensaje' => 'Ocurrió un inconveniente en la transacción de datos!','code'=>$e->getCode(), 'error'=>$e->getMessage()]);
        }
        DB::commit();
        return redirect()->back()->with(['mensaje' => 'Eliminación exitosa!', 'content' => $content]);
    }

    public function getContentImage($filename)
    {
        $imagen = Storage::disk('local')->get($filename);
        return new Response($imagen, 200);
    }
}
