<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Editor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard=null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->route('home_admin')->with(['mensaje'=>'Autorización negada!', 'error'=>'autorización negada']);
            }
        }else {
            if(Auth::guard($guard)->user()->role_id != 2 and Auth::guard($guard)->user()->role_id != 1){
                session()->flash('mensaje', 'No autorizado!');
                session()->flash('type', 'error');
                return redirect()->route('home_admin')->with(['mensaje'=>'Autorización negada!', 'error'=>'autorización negada']);
            }
        }
        return $next($request);
    }
}
