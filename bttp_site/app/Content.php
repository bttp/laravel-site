<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    //
    protected $fillable = [
        'title', 'content', 'sidebar', 'category_id','type','description', 'created_by'
    ];

    public function category()
    {
        return $this->belongsTo('\App\Category');
    }
}
